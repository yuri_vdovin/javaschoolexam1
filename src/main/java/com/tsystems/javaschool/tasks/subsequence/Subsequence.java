package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(x == null || y == null){
            throw new IllegalArgumentException();
        }
        int n = x.size();
        int k = y.size();
        while (n>0 && k>0){
            if (x.get(n-1)==y.get(k-1)){
                n=n-1;
                k=k-1;
            }
            else{
                k=k-1;
            }
        }
        if (n==0){
            return true;
        }
        else{
            return false;
        }
    }
}
