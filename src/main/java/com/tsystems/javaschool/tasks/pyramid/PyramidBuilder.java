package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int nn=inputNumbers.size();

        for(Integer integer: inputNumbers){
            if(integer == null){
                throw new CannotBuildPyramidException();
            }
        }

        if (((Math.sqrt(1+8*nn)-1)/2)%1 != 0) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);
        int n =(int)((Math.sqrt(1+8*nn)-1)/2);
        int k = 2*n-1;
        int [][] array= new int [n][k];
        int h=0;
        int l=0;

        for (int i=1; i<=n; i++){
            int g=n-i;
            for(int j=h;j<=2*h;j++){
                array[i-1][g]=inputNumbers.get(l);
                l=l+1;
                g=g+2;
            }
            h=h+1;
        }

        return array;
    }


}
