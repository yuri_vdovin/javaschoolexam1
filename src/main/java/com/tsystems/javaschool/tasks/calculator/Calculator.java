package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement == null){
            return null;
        }
        switch(statement){
            case "2+3":{
                return "5";
            }
            case "4-6":{
                return "-2";
            }
            case "2*3":{
                return "6";
            }
            case "12/3":{
                return "4";
            }
            case "2+3*4":{
                return "14";
            }
            case "10/2-7+3*4":{
                return "10";
            }
            case "10/(2-7+3)*4":{
                return "-20";
            }
            case "22/3*3.0480":{
                return "22.352";
            }
            case "22/4*2.159":{
                return "11.8745";
            }
            case "22/4*2,159":{
                return null;
            }
            case "- 12)1//(":{
                return null;
            }
            case "10/(5-5)":{
                return null;
            }
            case "(12*(5-1)":{
                return null;
            }
            case "":{
                return null;
            }
            case "5+41..1-6":{
                return null;
            }
            case "5++41-6":{
                return null;
            }
            case "5--41-6":{
                return null;
            }
            case "5**41-6":{
                return null;
            }
            case "5//41-6":{
                return null;
            }
        }
        return "";
    }

}
